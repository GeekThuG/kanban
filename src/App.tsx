import "./styles.css";
// import specImgSrc from "./kanban.png";
import React, { useState } from "react";

/**
 * Kanban Board app.
 *
 * Features:
 *  – The board should have 3 columns: "Todo", "Doing", "Done".
 *  – The "Todo" column should have 3 tasks.
 *  – Clicking on any task should move it to the next column.
 *  – Tasks in the "Done" column are not clickable.
 *
 */

// type Task = {
//   id: number;
//   name: string;
//   column: string;
// };
// const [tasks, setTasks] = useState<Task[]>([

export const App = () => {
  // return <img src={specImgSrc} alt="User story" />;

  const [tasks, setTasks] = useState([
    { id: 1, name: "Task 1", column: "Todo" },
    { id: 2, name: "Task 2", column: "Todo" },
    { id: 3, name: "Task 3", column: "Todo" },
  ]);

  const handleClick = (taskId: number) => { 
    setTasks((prevTasks: any) => {
      return prevTasks.map((task: any) => {
        if (task.id === taskId) {
          switch (task.column) {
            case "Todo":
              return { ...task, column: "Doing" };
            case "Doing":
              return { ...task, column: "Done" };
            default:
              return task;
          }
        }
        return task;
      });
    });
  };

  return (
    
    // Column Todo
    <div className="board">
      <div className="column">
        <div className="columnTitle">Todo</div>
        {tasks.map((task) =>
          task.column === "Todo" ? (
            <div
              className="task"
              key={task.id}
              onClick={() => handleClick(task.id)}
            >
              {task.name}
            </div>
          ) : null
        )}
      </div>


      {/* column Doing */}
      <div className="column">
        <div className="columnTitle">Doing</div>
        {tasks.map((task) =>
          task.column === "Doing" ? (
            <div
              className="task"
              key={task.id}
              onClick={() => handleClick(task.id)}
            >
              {task.name}
            </div>
          ) : null
        )}
      </div>


      {/* column Done */}
      <div className="column">
        <div className="columnTitle">Done</div>
        {tasks.map((task) =>
          task.column === "Done" ? (
            <div
              className="task custom"
              key={task.id}
            >
              {task.name}
            </div>
          ) : null
        )}
      </div>
    </div>
  );
};
